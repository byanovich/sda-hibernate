package ee.sda;

public class RegionDAO extends AbstractDAO<Region, Integer> {

    public RegionDAO() {
        super(Region.class);
    }
}
