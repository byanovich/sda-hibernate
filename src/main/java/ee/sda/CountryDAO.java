package ee.sda;

public class CountryDAO extends AbstractDAO<Country, String> {

    public CountryDAO() {
        super(Country.class);
    }
}
