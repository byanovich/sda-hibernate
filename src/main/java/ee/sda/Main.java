package ee.sda;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        DatabaseUtil.getSessionFactory();

        JobDAO jobDAO = new JobDAO();

        Job job = jobDAO.find(1);
        System.out.println(job);

        Job newJob = new Job();
        newJob.setTitle("New Job with hibernate");
        newJob.setMaxSalary(200000);
        newJob.setMinSalary(10000);

        System.out.println("Before save: " + newJob);
        newJob = jobDAO.save(newJob);
        System.out.println("After save: " + newJob);



        // newJob.setMinSalary(15000);
        //newJob.setLastUpdateTime();

        //jobDAO.update(newJob);

        Job anotherJob = new Job();
        // anotherJob.setId(21);
        anotherJob.setTitle("another title");
        anotherJob.setMinSalary(2);
        anotherJob.setMaxSalary(100);
        anotherJob = jobDAO.save(anotherJob);

        System.out.println(anotherJob);

        jobDAO.delete(anotherJob);

        RegionDAO regionDAO = new RegionDAO();

        Region firstRegion = regionDAO.find(1);

        System.out.println("First region: " + firstRegion);

        CountryDAO countryDAO = new CountryDAO();

        Country argentina = countryDAO.find("AR");

        System.out.println("Argentina: " + argentina);

//        Country morocco = new Country();
//        morocco.setId("MA");
//        morocco.setName("Morocco");
//        morocco.setRegionId(4);
//
//        countryDAO.save(morocco);

        System.out.println("Getting Morocco: " + countryDAO.find("MA"));

        List<Region> allRegions = regionDAO.findAll();

        System.out.println("All regions: ");
        for (Region region : allRegions) {
            System.out.println(region);
        }

        List<Job> jobsWith16000max = jobDAO.findJobsWithMaxSalary(16000);

        System.out.println("Job with max salary of 16000: ");
        for (Job job1 : jobsWith16000max) {
            System.out.println(job1);
        }

        List<Job> jobsThatPay5000 = jobDAO.findJobsWithSalary(5000);

        System.out.println("Jobs that pay 5000: ");
        for (Job job1 : jobsThatPay5000) {
            System.out.println(job1);
        }

        List<Job> allJobsWithNamedQuery = jobDAO.findAllWithNamedQuery();
        System.out.println("All jobs: ");
        for (Job job1 : allJobsWithNamedQuery) {
            System.out.println(job1);
        }

        jobsWith16000max = jobDAO.findJobsWithMaxSalaryNamedQuery(16000);
        for (Job job1 : jobsWith16000max) {
            System.out.println(job1);
        }

        jobsThatPay5000 = jobDAO.findJobsWithSalary(5000);

        System.out.println("Jobs that pay 5000: ");
        for (Job job1 : jobsThatPay5000) {
            System.out.println(job1);
        }

        Region europe = regionDAO.find(1);

        Country lithuania = new Country();
        lithuania.setId("LT");
        lithuania.setName("Lithuania");
        lithuania.setRegion(europe);

        countryDAO.save(lithuania);

        System.out.println("Europe region: " + europe);

        DatabaseUtil.shutdown();
    }

}
