package ee.sda;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import java.util.List;


public class JobDAO extends AbstractDAO<Job, Integer>{

    public JobDAO() {
        super(Job.class);
    }

    public List<Job> findJobsWithMaxSalary(double maxSalary) {
        Session session = DatabaseUtil.getSessionFactory().openSession();

        return session.createCriteria(Job.class).add(
                // not max_salary
                Restrictions.eq("maxSalary", maxSalary))
                .list();
    }

    // minSalary <= salary <= maxSalary
    // minSalary <= salary AND salary <= maxSalary
    public List<Job> findJobsWithSalary(double salary) {
        Session session = DatabaseUtil.getSessionFactory().openSession();

        // Restrictions.or
        // Restrictions.disjunction

        // Restrictions.and
        // Restrictions.conjunction

        return session.createCriteria(Job.class).add(Restrictions.conjunction(
                Restrictions.le("minSalary", salary),
                Restrictions.ge("maxSalary", salary)
        )).list();
    }


    public List<Job> findAllWithNamedQuery() {
        Session session = DatabaseUtil.getSessionFactory().openSession();
        Query query = session.createNamedQuery("find_all_jobs", Job.class);
        return query.list();
    }

    public List<Job> findJobsWithMaxSalaryNamedQuery(double salary) {
        Session session = DatabaseUtil.getSessionFactory().openSession();

        Query query = session.createNamedQuery("find_all_jobs_with_max_salary", Job.class);
        query.setParameter("salary", salary);

        return query.list();
    }

    public List<Job> findJobsWithSalaryQuery(double salary) {
        Session session = DatabaseUtil.getSessionFactory().openSession();

        Query query = session.createNamedQuery("find_all_jobs_with_salary", Job.class);
        query.setParameter("salary", salary);

        return query.list();
    }

}
