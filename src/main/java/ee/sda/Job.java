package ee.sda;

import javax.persistence.*;
import java.util.Objects;

@NamedQueries({
    @NamedQuery(name = "find_all_jobs", query = "FROM Job"),
    @NamedQuery(name = "find_all_jobs_with_max_salary",
            query = "FROM Job WHERE maxSalary = :salary"),
    @NamedQuery(name = "find_all_jobs_with_salary",
        query = "FROM Job WHERE minSalary <= :salary and maxSalary >= :salary")
})

@Entity
@Table(name = "jobs")
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "job_id")
    private int id;

    @Column(name = "job_title")
    private String title;

    @Column(name = "min_salary")
    private double minSalary;

    @Column(name = "max_salary")
    private double maxSalary;

    @PreRemove
    public void preDelete() {
        System.out.println("Calling preRemove");
    }

    @PrePersist
    public void prePersist() {
        title += "!!!";
        minSalary *= 1.1;

        System.out.println("Calling prePersist");
    }

    @PostPersist
    public void postPersist() {
        title += "???";
        System.out.println("Calling posrPersist");
    }

    @PreUpdate
    public void preUpdate() {
        System.out.println("Calling preUpdate");
    }

    @PostUpdate
    public void postUPdate() {
        System.out.println("Calling postUpdate");
    }

    @PostLoad
    public void postLoad() {
        System.out.println("Calling postLoad");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(double minSalary) {
        this.minSalary = minSalary;
    }

    public double getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(double maxSalary) {
        this.maxSalary = maxSalary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return id == job.id &&
                Double.compare(job.minSalary, minSalary) == 0 &&
                Double.compare(job.maxSalary, maxSalary) == 0 &&
                Objects.equals(title, job.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, minSalary, maxSalary);
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", minSalary=" + minSalary +
                ", maxSalary=" + maxSalary +
                '}';
    }
}
